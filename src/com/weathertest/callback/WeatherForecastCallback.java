package com.weathertest.callback;

import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.weathertest.api.Api;
import com.weathertest.api.DayWeather;
import com.weathertest.callback.WeatherForecastCallback.WeatherTaskParams;
import com.weathertest.util.Utils;

public abstract class WeatherForecastCallback extends
		AsyncTask<WeatherTaskParams, Void, ArrayList<DayWeather>> {

	private HttpClient client = new DefaultHttpClient();;
	private HttpGet httpGet;
	private ArrayList<DayWeather> dayWeather = new ArrayList<DayWeather>();

	@Override
	protected ArrayList<DayWeather> doInBackground(WeatherTaskParams... params) {
		try {
			String city = params[0].city;
			String dayCount = params[0].dayCount;

			httpGet = new HttpGet(Api.WEATHER_FORECAST + city
					+ "&units=metric&cnt=" + dayCount);

			HttpResponse response = client.execute(httpGet);
			String result = EntityUtils.toString(response.getEntity());
			Log.d("result", result + "");
			JSONObject jsonResponse = new JSONObject(result);

			JSONObject jsonCity = jsonResponse
					.getJSONObject(Api.WeatherForecast.city);
			String name = jsonCity.getString(Api.WeatherForecast.name);
			int id = jsonCity.getInt(Api.WeatherForecast.id);

			JSONObject jsonCoord = jsonCity
					.getJSONObject(Api.WeatherForecast.coord);
			double lang = jsonCoord.getDouble(Api.WeatherForecast.lat);
			double lat = jsonCoord.getDouble(Api.WeatherForecast.lon);

			JSONArray weatherDayList = (JSONArray) jsonResponse
					.get(Api.WeatherForecast.list);
			for (int i = 0; i < weatherDayList.length(); i++) {

				JSONObject listJsonObject = weatherDayList.getJSONObject(i);

				JSONObject tempJsonObject = listJsonObject
						.getJSONObject(Api.WeatherForecast.temp);
				double day = tempJsonObject.getDouble(Api.WeatherForecast.day);
				double min = tempJsonObject.getDouble(Api.WeatherForecast.min);
				double max = tempJsonObject.getDouble(Api.WeatherForecast.max);
				double night = tempJsonObject
						.getDouble(Api.WeatherForecast.night);
				double eve = tempJsonObject.getDouble(Api.WeatherForecast.eve);
				double morn = tempJsonObject
						.getDouble(Api.WeatherForecast.morn);

				double pressure = listJsonObject
						.getDouble(Api.WeatherForecast.pressure);
				double humidity = listJsonObject
						.getDouble(Api.WeatherForecast.humidity);
				int dateTime = listJsonObject
						.getInt(Api.WeatherForecast.dateTime);

				JSONArray weatherList = (JSONArray) listJsonObject
						.get(Api.WeatherForecast.weather);

				JSONObject weatherDesc = weatherList.getJSONObject(0);
				String main = weatherDesc.getString(Api.WeatherForecast.main);
				String description = weatherDesc
						.getString(Api.WeatherForecast.description);
				String icon = weatherDesc.getString(Api.WeatherForecast.icon);

				double speed = listJsonObject
						.getDouble(Api.WeatherForecast.speed);
				double deg = listJsonObject.getDouble(Api.WeatherForecast.deg);
				double clouds = listJsonObject
						.getDouble(Api.WeatherForecast.clouds);
				int rain = 0;

				if (listJsonObject.has(Api.WeatherForecast.rain))
					rain = listJsonObject.getInt(Api.WeatherForecast.rain);

				dayWeather.add(new DayWeather(day, min, max, night, eve, morn,
						pressure, humidity, main, description, icon, speed,
						deg, clouds, rain, dateTime));

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return dayWeather;
	}

	public abstract void onResponseReceived(ArrayList<DayWeather> result);

	@Override
	protected void onPostExecute(ArrayList<DayWeather> result) {
		result = dayWeather;
		onResponseReceived(result);
	}

	public interface RegisterCallbackIf {
		public void onResponseReceived(ArrayList<DayWeather> result);
	}

	public static class WeatherTaskParams {
		String city;
		String dayCount;

		public WeatherTaskParams(String city, String dayCount) {
			this.city = city;
			this.dayCount = dayCount;
		}
	}

}
