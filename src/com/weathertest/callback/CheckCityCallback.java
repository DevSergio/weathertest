package com.weathertest.callback;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.weathertest.api.Api;
import com.weathertest.util.Utils;

public abstract class CheckCityCallback extends
		AsyncTask<String, Void, Integer> {

	private HttpClient client = new DefaultHttpClient();;
	private HttpGet httpGet;
	private int status;

	@Override
	protected Integer doInBackground(String... params) {
		try {
			String city = params[0];

			httpGet = new HttpGet(Api.WEATHER_CURRENT + city + "&mode=json");

			HttpResponse response = client.execute(httpGet);
			String result = EntityUtils.toString(response.getEntity());
			Log.d("result", result + "");
			JSONObject jsonResponse = new JSONObject(result);

			status = jsonResponse.getInt(Api.WeatherCurrent.cod);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}

	public abstract void onResponseReceived(int result);

	@Override
	protected void onPostExecute(Integer result) {
		result = status;
		onResponseReceived(result);
	}

	public interface RegisterCallbackIf {

		public void onResponseReceived(int result);

	}

}
