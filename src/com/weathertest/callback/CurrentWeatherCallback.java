package com.weathertest.callback;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;

import com.weathertest.api.Api;
import com.weathertest.api.WeatherCurrent;

public abstract class CurrentWeatherCallback extends
		AsyncTask<String, Void, WeatherCurrent> {

	private HttpClient client = new DefaultHttpClient();;
	private HttpGet httpGet;
	private WeatherCurrent currentWeather;

	@Override
	protected WeatherCurrent doInBackground(String... params) {
		try {
			String city = params[0];

			httpGet = new HttpGet(Api.WEATHER_CURRENT + city + "&mode=json");

			HttpResponse response = client.execute(httpGet);
			String result = EntityUtils.toString(response.getEntity());
			Log.d("result", result + "");
			JSONObject jsonResponse = new JSONObject(result);

			int status = jsonResponse.getInt(Api.WeatherCurrent.cod);
			if (status != 404) {

				JSONObject infoObj = jsonResponse
						.getJSONObject(Api.WeatherCurrent.sys);
				String country = infoObj.getString(Api.WeatherForecast.country);
				int sunrise = infoObj.getInt(Api.WeatherCurrent.sunrise);
				int sunset = infoObj.getInt(Api.WeatherCurrent.sunset);

				JSONArray weatherList = (JSONArray) jsonResponse
						.get(Api.WeatherForecast.weather);

				JSONObject weatherDesc = weatherList.getJSONObject(0);
				String main = weatherDesc.getString(Api.WeatherForecast.main);
				String description = weatherDesc
						.getString(Api.WeatherForecast.description);
				String icon = weatherDesc.getString(Api.WeatherForecast.icon);

				JSONObject tempObj = jsonResponse
						.getJSONObject(Api.WeatherForecast.main);
				double temp = tempObj.getDouble(Api.WeatherForecast.temp);
				int pressure = tempObj.getInt(Api.WeatherForecast.pressure);
				int humidity = tempObj.getInt(Api.WeatherForecast.humidity);
				double tempMin = tempObj.getDouble(Api.WeatherCurrent.tempMin);
				double tempMax = tempObj.getDouble(Api.WeatherCurrent.tempMin);

				JSONObject windObj = jsonResponse
						.getJSONObject(Api.WeatherCurrent.wind);
				double speed = windObj.getDouble(Api.WeatherForecast.speed);
				int deg = windObj.getInt(Api.WeatherForecast.deg);

				JSONObject cloudsObj = jsonResponse
						.getJSONObject(Api.WeatherForecast.clouds);
				int clouds = cloudsObj.getInt(Api.WeatherCurrent.all);

				String currentCity = jsonResponse
						.getString(Api.WeatherForecast.name);

				currentWeather = new WeatherCurrent(country, sunrise, sunset,
						main, description, temp, pressure, humidity, tempMin,
						tempMax, speed, deg, currentCity, status);

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return currentWeather;
	}

	public abstract void onResponseReceived(WeatherCurrent result);

	@Override
	protected void onPostExecute(WeatherCurrent result) {
		result = currentWeather;
		onResponseReceived(result);
	}

	public interface RegisterCallbackIf {

		public void onResponseReceived(WeatherCurrent result);

	}

}
