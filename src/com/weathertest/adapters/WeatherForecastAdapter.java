package com.weathertest.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.weathertes.R;
import com.weathertest.api.DayWeather;
import com.weathertest.util.Utils;

public class WeatherForecastAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private Context context;
	private ArrayList<DayWeather> weatherForecasts;

	public WeatherForecastAdapter(ArrayList<DayWeather> weatherForecasts,
			Context context) {
		this.weatherForecasts = weatherForecasts;
		this.context = context;
		this.inflater = LayoutInflater.from(this.context);

	}

	@Override
	public int getCount() {
		return weatherForecasts.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.weather_forecast_row,
					parent, false);
			viewHolder = new ViewHolder(convertView);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		DayWeather weatherForecast = weatherForecasts.get(position);

		viewHolder.weather_forecast_row_day.setText(context
				.getString(R.string.weather_forecast_row_day)
				+ weatherForecast.getDay());
		viewHolder.weather_forecast_row_night.setText(context
				.getString(R.string.weather_forecast_row_night)
				+ weatherForecast.getNight());
		viewHolder.weather_forecast_row_eve.setText(context
				.getString(R.string.weather_forecast_row_evening)
				+ weatherForecast.getEve());
		viewHolder.weather_forecast_row_morn.setText(context
				.getString(R.string.weather_forecast_row_morning)
				+ weatherForecast.getMorn());
		viewHolder.weather_forecast_row_pressure.setText(context
				.getString(R.string.weather_forecast_row_pressure)
				+ weatherForecast.getPressure());
		viewHolder.weather_forecast_row_wind.setText(context
				.getString(R.string.weather_forecast_row_wind)
				+ weatherForecast.getSpeed());
		viewHolder.weather_forecast_row_date.setText(context
				.getString(R.string.weather_forecast_row_date)
				+ Utils.convertTimestampToDate(weatherForecast.getDateTime()));

		return convertView;
	}

	private static class ViewHolder {
		private TextView weather_forecast_row_day;
		private TextView weather_forecast_row_night;
		private TextView weather_forecast_row_eve;
		private TextView weather_forecast_row_morn;
		private TextView weather_forecast_row_pressure;
		private TextView weather_forecast_row_wind;
		private TextView weather_forecast_row_date;

		public ViewHolder(View convertView) {

			weather_forecast_row_day = (TextView) convertView
					.findViewById(R.id.weather_forecast_row_day);
			weather_forecast_row_night = (TextView) convertView
					.findViewById(R.id.weather_forecast_row_night);
			weather_forecast_row_eve = (TextView) convertView
					.findViewById(R.id.weather_forecast_row_evening);
			weather_forecast_row_morn = (TextView) convertView
					.findViewById(R.id.weather_forecast_row_morning);
			weather_forecast_row_pressure = (TextView) convertView
					.findViewById(R.id.weather_forecast_row_pressure);
			weather_forecast_row_wind = (TextView) convertView
					.findViewById(R.id.weather_forecast_row_wind);
			weather_forecast_row_date = (TextView) convertView
					.findViewById(R.id.weather_forecast_row_date);

		}
	}

}
