package com.weathertest.util;

import java.util.Date;

public class Utils {

	public static Date convertTimestampToDate(int timestamp) {
		Date time = new java.util.Date((long) timestamp * 1000);
		return time;

	}

}
