package com.weathertest.app;

import java.util.List;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.weathertes.R;
import com.weathertest.app.db.WeatherCity;
import com.weathertest.app.db.WeatherDataSource;
import com.weathertest.callback.CheckCityCallback;

public class MainActivity extends ListActivity implements OnClickListener {

	private ListView act_main_list;
	private EditText act_main_edittext;
	private WeatherDataSource datasource;
	private WeatherCity weatherCity;
	private ArrayAdapter<WeatherCity> adapter;
	private AsyncTask<String, Void, Integer> currentWeatherCallback;
	private String city;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		act_main_edittext = (EditText) findViewById(R.id.activity_main_edittext);
		act_main_list = getListView();

		act_main_list.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					final int pos, long arg3) {
				new AlertDialog.Builder(MainActivity.this)
						.setMessage(
								"Are you sure you want to delete this city?")
						.setCancelable(false)
						.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										weatherCity = (WeatherCity) getListAdapter()
												.getItem(pos);
										datasource.deleteCity(weatherCity);
										adapter.remove(weatherCity);

									}
								}).setNegativeButton("No", null).show();

				return false;
			}
		});

		act_main_list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int pos,
					long arg3) {
				Object obj = act_main_list.getAdapter().getItem(pos);
				String value = obj.toString();
				CurrentWeatherActivity.StartNew(MainActivity.this, value);
			}
		});

		datasource = new WeatherDataSource(this);
		datasource.open();

		List<WeatherCity> values = datasource.getAll�ities();

		adapter = new ArrayAdapter<WeatherCity>(this,
				android.R.layout.simple_list_item_1, values);
		if (values.size() == 0)
			addDefaultCity();

		setListAdapter(adapter);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onResume() {
		datasource.open();
		super.onResume();
	}

	@Override
	protected void onPause() {
		datasource.close();
		super.onPause();
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.activity_main_button_add:
			city = act_main_edittext.getText().toString();
			if (!city.isEmpty()) {
				currentWeatherCallback = new CheckCityCallback() {

					@Override
					public void onResponseReceived(int result) {
						switch (result) {
						case 200:
							weatherCity = datasource.createCity(city);
							adapter.add(weatherCity);
							act_main_edittext.setText("");
							break;
						case 404:
							Toast.makeText(MainActivity.this, "Incorrect city",
									Toast.LENGTH_SHORT).show();
							break;
						}
						Log.d("Switch result", result + "");

					}
				}.execute(city);
			}
			break;
		}

	}

	public void addDefaultCity() {
		String[] city = { "Moscow", "Kiev" };
		for (int i = 0; i < city.length; i++) {

			weatherCity = datasource.createCity(city[i]);

			adapter.add(weatherCity);
		}
	}
}
