package com.weathertest.app.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class WeatherDataSource {

	private SQLiteDatabase database;
	private SQLiteHelper dbHelper;
	private String[] allColumns = { SQLiteHelper.COLUMN_ID,
			SQLiteHelper.COLUMN_CITY };

	public WeatherDataSource(Context context) {
		dbHelper = new SQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public WeatherCity createCity(String city) {
		ContentValues values = new ContentValues();
		values.put(SQLiteHelper.COLUMN_CITY, city);
		long insertId = database.insert(SQLiteHelper.TABLE_WEATHER, null,
				values);
		Cursor cursor = database.query(SQLiteHelper.TABLE_WEATHER,
				allColumns, SQLiteHelper.COLUMN_ID + " = " + insertId, null,
				null, null, null);
		cursor.moveToFirst();
		WeatherCity newCity = cursorToCity(cursor);
		cursor.close();
		return newCity;
	}

	public void deleteCity(WeatherCity comment) {
		long id = comment.getId();
		System.out.println("Comment deleted with id: " + id);
		database.delete(SQLiteHelper.TABLE_WEATHER, SQLiteHelper.COLUMN_ID
				+ " = " + id, null);
	}

	public List<WeatherCity> getAll�ities() {
		List<WeatherCity> cities = new ArrayList<WeatherCity>();

		Cursor cursor = database.query(SQLiteHelper.TABLE_WEATHER,
				allColumns, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			WeatherCity comment = cursorToCity(cursor);
			cities.add(comment);
			cursor.moveToNext();
		}
		cursor.close();
		return cities;
	}

	private WeatherCity cursorToCity(Cursor cursor) {
		WeatherCity weatherCity = new WeatherCity();
		weatherCity.setId(cursor.getLong(0));
		weatherCity.setCity(cursor.getString(1));
		return weatherCity;
	}
}
