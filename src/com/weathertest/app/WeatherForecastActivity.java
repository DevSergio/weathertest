package com.weathertest.app;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ListView;

import com.example.weathertes.R;
import com.weathertest.adapters.WeatherForecastAdapter;
import com.weathertest.api.DayWeather;
import com.weathertest.callback.WeatherForecastCallback;
import com.weathertest.callback.WeatherForecastCallback.WeatherTaskParams;

public class WeatherForecastActivity extends Activity {

	private ListView weather_forecast_list;
	private AsyncTask<WeatherTaskParams, Void, ArrayList<DayWeather>> currentWeatherCallback;
	private WeatherForecastAdapter weather_forecast_adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.weather_forecast);

		weather_forecast_list = (ListView) findViewById(R.id.weather_forecast_list);

		Bundle extras = getIntent().getExtras();
		String days = extras.getString(KEY_DAYS);
		String city = extras.getString(KEY_CITY);

		WeatherTaskParams weatherTaskParams = new WeatherTaskParams("Kiev", days);
		currentWeatherCallback = new WeatherForecastCallback() {

			@Override
			public void onResponseReceived(ArrayList<DayWeather> result) {
				weather_forecast_adapter = new WeatherForecastAdapter(result,
						getApplicationContext());
				weather_forecast_list.setAdapter(weather_forecast_adapter);
			}
		}.execute(weatherTaskParams);

	}

	private static final String KEY_DAYS = "KEY_DAYS";
	private static final String KEY_CITY = "KEY_CITY";

	public static void StartNew(Context context, String days, String city) {
		Intent intent = new Intent(context, WeatherForecastActivity.class);
		if (days != null) {
			intent.putExtra(KEY_DAYS, days);
		}
		if (city != null) {
			intent.putExtra(KEY_CITY, city);
		}

		context.startActivity(intent);
	}
}
