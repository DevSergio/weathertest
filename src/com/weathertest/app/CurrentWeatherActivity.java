package com.weathertest.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.weathertes.R;
import com.weathertest.api.WeatherCurrent;
import com.weathertest.callback.CurrentWeatherCallback;

public class CurrentWeatherActivity extends Activity implements OnClickListener {

	private Spinner current_weather_city_spinner;
	private TextView current_weather_city_name;
	private TextView current_weather_city_temp;
	private TextView current_weather_city_temp_min;
	private TextView current_weather_city_temp_max;
	private TextView current_weather_city_humidity;
	private TextView current_weather_city_pressure;
	private TextView current_weather_city_wind;
	private AsyncTask<String, Void, WeatherCurrent> currentWeatherCallback;
	private String spinnerValue;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.current_weather_city);

		current_weather_city_name = (TextView) findViewById(R.id.current_weather_city_name);
		current_weather_city_temp = (TextView) findViewById(R.id.current_weather_city_temp);
		current_weather_city_temp_min = (TextView) findViewById(R.id.current_weather_city_temp_min);
		current_weather_city_temp_max = (TextView) findViewById(R.id.current_weather_city_temp_max);
		current_weather_city_humidity = (TextView) findViewById(R.id.current_weather_city_humidity);
		current_weather_city_pressure = (TextView) findViewById(R.id.current_weather_city_pressure);
		current_weather_city_wind = (TextView) findViewById(R.id.current_weather_city_wind);
		current_weather_city_spinner = (Spinner) findViewById(R.id.current_weather_city_spinner);

		Bundle extras = getIntent().getExtras();
		String city = extras.getString(KEY_CITY);

		currentWeatherCallback = new CurrentWeatherCallback() {

			@Override
			public void onResponseReceived(WeatherCurrent result) {
				current_weather_city_name
						.setText(getString(R.string.current_weather_city_name)
								+ " " + result.getName());
				current_weather_city_temp
						.setText(getString(R.string.current_weather_city_temp)
								+ " " + result.getTemp());
				current_weather_city_temp_min
						.setText(getString(R.string.current_weather_city_min)
								+ " " + result.getTempMin());
				current_weather_city_temp_max
						.setText(getString(R.string.current_weather_city_max)
								+ " " + result.getTempMax());
				current_weather_city_humidity
						.setText(getString(R.string.current_weather_city_humidity)
								+ " " + result.getHumidity());
				current_weather_city_pressure
						.setText(getString(R.string.current_weather_city_pressure)
								+ " " + result.getPressure());
				current_weather_city_wind
						.setText(getString(R.string.current_weather_city_wind)
								+ " " + result.getWindSpeed());
			}
		}.execute(city);

		current_weather_city_spinner
				.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> parent,
							View view, int pos, long id) {
						Object item = parent.getItemAtPosition(pos);
						spinnerValue = item.toString();
						Log.d("Spinner item", item + "");
					}

					public void onNothingSelected(AdapterView<?> parent) {
					}
				});

		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				this, R.array.days_array, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		current_weather_city_spinner.setAdapter(adapter);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.current_weather_city_show:
			WeatherForecastActivity.StartNew(CurrentWeatherActivity.this,
					spinnerValue, "Moscow");

		default:
			break;
		}
	}

	private static final String KEY_CITY = "KEY_CITY";

	public static void StartNew(Context context, String city) {
		Intent intent = new Intent(context, CurrentWeatherActivity.class);
		if (city != null) {
			intent.putExtra(KEY_CITY, city);
		}

		context.startActivity(intent);
	}
}
