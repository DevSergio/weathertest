package com.weathertest.api;

import java.util.ArrayList;

public class WeatherForecast {

	private ArrayList<DayWeather> dayWeather;

	public WeatherForecast(ArrayList<DayWeather> dayWeather) {
		this.dayWeather = dayWeather;
	}

	public ArrayList<DayWeather> getDayWeather() {
		return dayWeather;
	}

	public void setDayWeather(ArrayList<DayWeather> dayWeather) {
		this.dayWeather = dayWeather;
	}

}
