package com.weathertest.api;

public class Api {

	public static final String WEATHER_FORECAST = "http://api.openweathermap.org/data/2.5/forecast/daily?q=";
	public static final String WEATHER_CURRENT = "http://api.openweathermap.org/data/2.5/weather?q=";

	public static class WeatherForecast {
		public static final String city = "city";
		public static final String id = "id";
		public static final String name = "name";
		public static final String country = "country";
		public static final String population = "population";

		public static final String coord = "coord";
		public static final String lon = "lon";
		public static final String lat = "lat";

		public static final String list = "list";
		public static final String temp = "temp";
		public static final String day = "day";
		public static final String min = "min";
		public static final String max = "max";
		public static final String night = "night";
		public static final String eve = "eve";
		public static final String morn = "morn";
		public static final String dateTime = "dt";

		public static final String pressure = "pressure";
		public static final String humidity = "humidity";

		public static final String weather = "weather";
		public static final String weatherId = "id";
		public static final String main = "main";
		public static final String description = "description";
		public static final String icon = "icon";

		public static final String speed = "speed";
		public static final String deg = "deg";
		public static final String clouds = "clouds";
		public static final String rain = "rain ";

	}

	public static class WeatherCurrent {
		public static final String sys = "sys";
		public static final String sunrise = "sunrise";
		public static final String sunset = "sunset";
		public static final String tempMin = "temp_min";
		public static final String tempMax = "temp_max";
		public static final String wind = "wind";
		public static final String all = "all";
		public static final String cod = "cod";
	}
}
