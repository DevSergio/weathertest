package com.weathertest.api;

public class DayWeather {

	private double day;
	private double min;
	private double max;
	private double night;
	private double eve;
	private double morn;
	private double pressure;
	private double humidity;
	private String main;
	private String description;
	private String icon;
	private double speed;
	private double deg;
	private double clouds;
	private int rain;
	private int dt;

	public DayWeather(double day, double min, double max, double night,
			double eve, double morn, double pressure, double humidity,
			String main, String description, String icon, double speed,
			double deg, double clouds, int rain, int dt) {
		this.day = day;
		this.min = min;
		this.max = max;
		this.night = night;
		this.eve = eve;
		this.morn = morn;
		this.pressure = pressure;
		this.humidity = humidity;
		this.main = main;
		this.description = description;
		this.icon = icon;
		this.speed = speed;
		this.deg = deg;
		this.clouds = clouds;
		this.rain = rain;
		this.dt = dt;
	}

	public double getDay() {
		return day;

	}

	public void setDay(double day) {
		this.day = day;
	}

	public double getMin() {
		return min;
	}

	public void setMin(double min) {
		this.min = min;
	}

	public double getMax() {
		return max;
	}

	public void setMax(double max) {
		this.max = max;
	}

	public double getNight() {
		return night;
	}

	public void setNight(double night) {
		this.night = night;
	}

	public double getEve() {
		return eve;
	}

	public void setEve(double eve) {
		this.eve = eve;
	}

	public double getMorn() {
		return morn;
	}

	public void setMorn(double morn) {
		this.morn = morn;
	}

	public double getPressure() {
		return pressure;
	}

	public void setPressure(double pressure) {
		this.pressure = pressure;
	}

	public double getHumidity() {
		return humidity;
	}

	public void setHumidity(double humidity) {
		this.humidity = humidity;
	}

	public String getMain() {
		return main;
	}

	public void setMain(String main) {
		this.main = main;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public double getSpeed() {
		return speed;
	}

	public void setSpeed(double speed) {
		this.speed = speed;
	}

	public double getDeg() {
		return deg;
	}

	public void setDeg(double deg) {
		this.deg = deg;
	}

	public double getClouds() {
		return clouds;
	}

	public void setClouds(double clouds) {
		this.clouds = clouds;
	}

	public int getRain() {
		return rain;
	}

	public void setRain(int rain) {
		this.rain = rain;
	}

	public int getDateTime() {
		return dt;
	}

	public void setDateTime(int dt) {
		this.dt = dt;
	}
}
